var apiResponse = {
    "inheritanceForecast": [
        {
            "Year": 1,
            "netEstate": 1161252.5633987768,
            "IHTtax": 425803.3184366592
        },
        {
            "Year": 2,
            "netEstate": 1191981.4626418976,
            "IHTtax": 422795.56067178573
        },
        {
            "Year": 3,
            "netEstate": 1220526.5208531625,
            "IHTtax": 422831.3516049301
        },
        {
            "Year": 4,
            "netEstate": 1255128.1736364653,
            "IHTtax": 417218.936382839
        },
        {
            "Year": 5,
            "netEstate": 1286792.3831545073,
            "IHTtax": 415245.2737492973
        },
        {
            "Year": 6,
            "netEstate": 1334638.6128803908,
            "IHTtax": 396018.89660673746
        },
        {
            "Year": 7,
            "netEstate": 1432063.731338322,
            "IHTtax": 321008.4992954398
        },
        {
            "Year": 8,
            "netEstate": 1462288.2222170166,
            "IHTtax": 320130.43614176515
        },
        {
            "Year": 9,
            "netEstate": 1492791.031337443,
            "IHTtax": 319240.2364517404
        },
        {
            "Year": 10,
            "netEstate": 1663564.822219986,
            "IHTtax": 150994.3546393567
        },
        {
            "Year": 11,
            "netEstate": 1688127.3347733915,
            "IHTtax": 153960.2379976558
        },
        {
            "Year": 12,
            "netEstate": 1712931.989447832,
            "IHTtax": 156983.22874660924
        },
        {
            "Year": 13,
            "netEstate": 1729512.5460765574,
            "IHTtax": 158061.0121423626
        },
        {
            "Year": 14,
            "netEstate": 1746209.2871233767,
            "IHTtax": 159190.94497841268
        },
        {
            "Year": 15,
            "netEstate": 1763009.6001761176,
            "IHTtax": 160372.6001900043
        },
        {
            "Year": 16,
            "netEstate": 1779902.5108723468,
            "IHTtax": 161605.44655061176
        },
        {
            "Year": 17,
            "netEstate": 1796878.4903976496,
            "IHTtax": 162888.86422490366
        },
        {
            "Year": 18,
            "netEstate": 1813929.2830855737,
            "IHTtax": 164222.15884443594
        },
        {
            "Year": 19,
            "netEstate": 1831047.752568772,
            "IHTtax": 165604.57416663112
        },
        {
            "Year": 20,
            "netEstate": 1848227.7449105743,
            "IHTtax": 167035.30339480884
        },
        {
            "Year": 21,
            "netEstate": 1865463.9671729498,
            "IHTtax": 168513.4992474974
        },
        {
            "Year": 22,
            "netEstate": 1882751.8799363126,
            "IHTtax": 170038.2828706167
        },
        {
            "Year": 23,
            "netEstate": 1900087.6023676293,
            "IHTtax": 171608.75168770144
        },
        {
            "Year": 24,
            "netEstate": 1917467.8285273423,
            "IHTtax": 173223.98628216278
        },
        {
            "Year": 25,
            "netEstate": 1934889.7537062292,
            "IHTtax": 174883.05640247444
        },
        {
            "Year": 26,
            "netEstate": 1952351.0096857839,
            "IHTtax": 176585.0261767537
        },
        {
            "Year": 27,
            "netEstate": 1969849.6079166995,
            "IHTtax": 178328.95861796275
        },
        {
            "Year": 28,
            "netEstate": 1987383.8897071704,
            "IHTtax": 180113.91949524844
        },
        {
            "Year": 29,
            "netEstate": 2004952.4826045944,
            "IHTtax": 181938.98064104293
        },
        {
            "Year": 30,
            "netEstate": 2022554.262239895,
            "IHTtax": 183803.2227576547
        }
    ],
    "inheritanceTaxComparison": [
        {
            "Year": 1,
            "IHTwithPlanning": 425803.3184366592,
            "IHTwithoutPlanning": 432192.91560451465
        },
        {
            "Year": 2,
            "IHTwithPlanning": 422795.56067178573,
            "IHTwithoutPlanning": 435553.7906221482
        },
        {
            "Year": 3,
            "IHTwithPlanning": 422831.3516049301,
            "IHTwithoutPlanning": 443533.0807992811
        },
        {
            "Year": 4,
            "IHTwithPlanning": 417218.936382839,
            "IHTwithoutPlanning": 447694.28125294606
        },
        {
            "Year": 5,
            "IHTwithPlanning": 415245.2737492973,
            "IHTwithoutPlanning": 455675.52616679686
        },
        {
            "Year": 6,
            "IHTwithPlanning": 396018.89660673746,
            "IHTwithoutPlanning": 463647.4599918064
        },
        {
            "Year": 7,
            "IHTwithPlanning": 321008.4992954398,
            "IHTwithoutPlanning": 382334.37715823035
        },
        {
            "Year": 8,
            "IHTwithPlanning": 320130.43614176515,
            "IHTwithoutPlanning": 392156.5291846567
        },
        {
            "Year": 9,
            "IHTwithPlanning": 319240.2364517404,
            "IHTwithoutPlanning": 402032.09635123215
        },
        {
            "Year": 10,
            "IHTwithPlanning": 150994.3546393567,
            "IHTwithoutPlanning": 411957.9329580983
        },
        {
            "Year": 11,
            "IHTwithPlanning": 153960.2379976558,
            "IHTwithoutPlanning": 421931.18563110376
        },
        {
            "Year": 12,
            "IHTwithPlanning": 156983.22874660924,
            "IHTwithoutPlanning": 431949.26951068523
        },
        {
            "Year": 13,
            "IHTwithPlanning": 158061.0121423626,
            "IHTwithoutPlanning": 438005.056619571
        },
        {
            "Year": 14,
            "IHTwithPlanning": 159190.94497841268,
            "IHTwithoutPlanning": 444091.3308203801
        },
        {
            "Year": 15,
            "IHTwithPlanning": 160372.6001900043,
            "IHTwithoutPlanning": 450205.81250092905
        },
        {
            "Year": 16,
            "IHTwithPlanning": 161605.44655061176,
            "IHTwithoutPlanning": 456346.45891672483
        },
        {
            "Year": 17,
            "IHTwithPlanning": 162888.86422490366,
            "IHTwithoutPlanning": 462511.4415343522
        },
        {
            "Year": 18,
            "IHTwithPlanning": 164222.15884443594,
            "IHTwithoutPlanning": 468699.12545683334
        },
        {
            "Year": 19,
            "IHTwithPlanning": 165604.57416663112,
            "IHTwithoutPlanning": 474908.05075808737
        },
        {
            "Year": 20,
            "IHTwithPlanning": 167035.30339480884,
            "IHTwithoutPlanning": 481136.91556372243
        },
        {
            "Year": 21,
            "IHTwithPlanning": 168513.4992474974,
            "IHTwithoutPlanning": 487384.5607258938
        },
        {
            "Year": 22,
            "IHTwithPlanning": 170038.2828706167,
            "IHTwithoutPlanning": 493649.95595054264
        },
        {
            "Year": 23,
            "IHTwithPlanning": 171608.75168770144,
            "IHTwithoutPlanning": 499932.1872457971
        },
        {
            "Year": 24,
            "IHTwithPlanning": 173223.98628216278,
            "IHTwithoutPlanning": 506230.44557052356
        },
        {
            "Year": 25,
            "IHTwithPlanning": 174883.05640247444,
            "IHTwithoutPlanning": 512544.01657179906
        },
        {
            "Year": 26,
            "IHTwithPlanning": 176585.0261767537,
            "IHTwithoutPlanning": 518872.2713094001
        },
        {
            "Year": 27,
            "IHTwithPlanning": 178328.95861796275,
            "IHTwithoutPlanning": 525214.6578741818
        },
        {
            "Year": 28,
            "IHTwithPlanning": 180113.91949524844,
            "IHTwithoutPlanning": 531570.6938154371
        },
        {
            "Year": 29,
            "IHTwithPlanning": 181938.98064104293,
            "IHTwithoutPlanning": 537939.9592999708
        },
        {
            "Year": 30,
            "IHTwithPlanning": 183803.2227576547,
            "IHTwithoutPlanning": 544322.090932689
        }
    ],
    "inheritanceTaxBreakdownComparison": [
        {
            "IHTplanning": "With",
            "taxMan": 167000,
            "beneficiary": 462000
        },
        {
            "IHTplanning": "Without",
            "taxMan": 481000,
            "beneficiary": 411000
        }
    ]
}