const puppeteer = require('puppeteer');
const { startServer } = require('polyserve');
const path = require('path');
const fs = require('fs');
const baselineDir = `${process.cwd()}/test/integration/screenshots-baseline`;

describe('regenerate screenshots for testing purposes', function () {
  let polyserve, browser, page;

  before(async function () {
    polyserve = await startServer({ port: 4444, root: path.join(__dirname, '../../..'), moduleResolution: 'node' });

    // Create the test directory if needed.
    if (!fs.existsSync(baselineDir)) {
      fs.mkdirSync(baselineDir);
    }
  });

  after((done) => polyserve.close(done));

  beforeEach(async function () {
    browser = await puppeteer.launch();
    page = await browser.newPage();
  });

  afterEach(() => browser.close());

  it('did it', async function () {
    return generateBaselineScreenshots(page);
  });
});

async function generateBaselineScreenshots(page) {
  // demo
  await page.goto('http://127.0.0.1:4444/components/ev-charts/demo');
  await page.waitFor(5000);
  await page.screenshot({ path: `${baselineDir}/index.png` });
}
